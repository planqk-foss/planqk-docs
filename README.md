# PLANQK Documentation

Documentation for the PLANQK Platform available at <https://docs.planqk.de>.

## Build and run the project

```bash
npm install
npm run docs:dev
# or
npm run docs:build
```

## License

CC-BY-4.0 | Copyright 2022-2024 Kipu Quantum GmbH
