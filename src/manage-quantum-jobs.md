# Manage Quantum Jobs

Gain a comprehensive overview of all quantum jobs or tasks you have submitted using the PLANQK SDK by visiting the [Quantum Jobs](https://platform.planqk.de/quantum-jobs) page.

If you need to view the jobs submitted by an organization you are a member of, simply switch your account context by clicking on your name in the **upper right corner** of the page.
  
## Job Actions

By clicking the action button on the right side of each job, you can perform the following actions:

- **Retrieve Inputs & Results**:  
  Download your quantum job inputs and results (after the job has completed) directly through the UI.

- **Cancel Jobs**:  
  Cancel jobs that are still queued at the backend. This feature helps you save costs, especially if expensive jobs are accidentally submitted to costly backends.

<ImageShadow src="/images/quantum-jobs/action-menu.png"></ImageShadow>

## Managing Service Jobs

As a service host, you can view the input data and results of jobs initiated by your service’s executions. 
Additionally, you have the ability to cancel any jobs that are queued from a service execution.

Follow these steps to view jobs associated with a specific service execution:

1. Click on "Applications" tab in the main menu. 
If you want to access the jobs of your organization, ensure that you selected it, in the top left corner.
2. Choose your subscribed service from the list of subscribed services.
3. In the "Subscriptions" section, click on "Activity Logs".
4. Locate the relevant service execution and click on "Show Jobs".

<ImageShadow src="/images/quantum-jobs/service-execution-jobs.png"></ImageShadow>


::: tip NOTE
Due to confidentiality reasons, you **cannot** access jobs from service executions initiated by external users or organizations, even if you are hosting the service.
:::

