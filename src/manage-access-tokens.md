# Manage Access Tokens

In PLANQK, access tokens are used in token-based authentications to allow users to access the PLANQK API or to let PLANQK at runtime access the API of a quantum backend provider.

PLANQK supports two types of access tokens:
(1) **Personal Access Tokens**
for accessing the PLANQK API, e.g., by the [PLANQK CLI](cli-reference) to automate the interaction with the PLANQK Platform or by the [PLANQK Quantum SDK](sdk-reference) to develop and execute quantum circuits using our Qiskit extension, and
(2) **Provider Access Tokens**
to allow PLANQK accessing the API of quantum backend providers at runtime.
This is especially useful when you want to execute your quantum solutions using your own accounts for certain quantum backends (bring your own token).

## Personal Access Tokens

You can use personal access tokens to access the PLANQK API, e.g., by the [PLANQK CLI](cli-reference) or by the [PLANQK Quantum SDK](sdk-reference).
Further, you can use them to authenticate any custom application that wants to interact with the PLANQK Platform API.

<ImageShadow src="/images/create-personal-access-token.png"></ImageShadow>

To create a personal access token to your account, go to the user-menu in the top right corner and click on "Settings".
Under "Personal Access Tokens" you can create new personal access tokens and manage existing ones.

::: tip NOTE
Personal access tokens can only be created for user accounts.
You can use your personal access token to interact with organizations you are a member of.
:::

## Provider Access Tokens

By bringing your own access tokens, you can use your own accounts for certain quantum backends.
This allows PLANQK to access quantum backend providers at runtime.

<ImageShadow src="/images/manage-provider-access-tokens.png"></ImageShadow>

To add a token for your account, go to the user-menu in the top right corner and click on "Settings".
Under "Provider Access Tokens" you can add different tokens to your account, depending on the provider.

Alternatively, when you are an owner or maintainer of an organization, you can provide access tokens in the section "Provider Access Tokens" of your organization settings.
If provided, every member of the organization can run circuits/jobs with these access tokens.
