# Introduction

Implementations are hosted as Git repositories, which means that version control and collaboration are core elements of PLANQK.
In a nutshell, an implementation (also known as a repo or repository) is a place where code and assets can be stored to back up your work, share it with the community, and work in a team.

In these pages, you will go over the basics of getting started with Git and interacting with implementations on PLANQK.

## What's next?

- [Getting Started](getting-started)
- [Implementation Settings](settings)
- [Create a PLANQK Service](create-a-service)
