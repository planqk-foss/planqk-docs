# What is the Community Platform?

Here, the quantum community comes together for exchanging their knowledge and experience. Whether you are interested in the key concepts of certain algorithms, want to see implementations (for various SDKs) or learn about real world use cases for quantum computers, this is your place to be.  
This part of the PLANQK Platform is driven by various experts from industry and academia to ensure a high quality standard and up-to-date information for this dynamically developing field.

We invite all users to actively create content and [to publish](publish-content) it to make it available to the PLANQK community.
Alternatively, you can [share](manage-permissions) your content only with a specific user group.

You can also participate by discussing existing content.
Simply click on "Discussions" tab of the content at hand, which shows past and ongoing threads regarding aspects of e.g. the algorithm.
After opening a new discussion in the top right corner, the authors will be notified and hopefully are able to help you.

To ensure theoretical and technical soundness of the content, the experts evaluate each other's contents in the form of [reviews](reviews).
You can identify reviewed content in the platform by a golden ribbon attached to it.
