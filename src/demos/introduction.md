# Introduction

PLANQK Demos make it easy for you to create and host interactive web interfaces for your quantum and machine learning use cases.
To deploy a Demo, simply connect a GitHub or GitLab repository.
PLANQK automatically builds and deploys your Demo every time you push to the default branch of your repository.

A simple way to create a Demo is to use our [Gradio starter template](https://github.com/planqk/planqk-demo-starter-gradio).
But, you can also deploy any other web app of your choice using Docker.
