# Accessing Services from your Python Code

The PLANQK Service SDK enables you to access and monitor your deployed services directly from a Python program.
Supported operations are: creating, monitoring the status of the service, retrieving the execution result and cancelling the execution of the service.

Begin by installing the PLANQK Service SDK using pip.

```bash
pip install --upgrade planqk-service-sdk
```

Replace the placeholders `your_consumer_key` and `your_secret_key` in the code snippet below with the credentials provided in one of your platform applications.
Additionally, configure the `service_endpoint` to the endpoint URL of the subscribed PLANQK service (see list of subscriptions in an application).

```python
from planqk.service.client import PlanqkServiceClient

# Your consumer key and secret
consumer_key = "your_consumer_key"
consumer_secret = "your_consumer_secret"

# Service endpoint
service_endpoint = "https://gateway.platform.planqk.de/..."

# Initialize the client
client = PlanqkServiceClient(service_endpoint, consumer_key, consumer_secret)
```

You can initiate executions as illustrated in the following example:

```python
data = {"dataValue" : ["abc", "dce"]}
params = {"paramsValue" : ["abc", "dce"]}

job = client.start_execution(data=data, params=params)
```

To check the status of an execution, utilize the following code snippet:

```python
status = client.get_status(job.id)
```

Retrieve the result of an execution with the following example:

```python
result = client.get_result(job.id)
```

Lastly, to cancel an execution, follow this example:

```python
client.cancel_execution(job.id)
```
