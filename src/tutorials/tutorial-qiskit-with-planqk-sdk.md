# Execute Qiskit Circuits using the PLANQK Quantum SDK

This tutorial describes how you can use the PLANQK Quantum SDK to execute your Qiskit code on different quantum backends supported by PLANQK.
The SDK is a wrapper for Qiskit 1.0.
Hence, it provides the same functionality and syntax as the original Qiskit SDK.

You can use the SDK either directly from your favorite IDE or in a [PLANQK service](../services/managed/introduction).

## Install the PLANQK Quantum SDK

To install the PLANQK Quantum SDK you need to have Python 3.11 or higher installed.
The package is released on PyPI and can be installed via `pip`:

```bash
pip install planqk-quantum
```

## Create an Access Token

To access the quantum backends from your Qiskit code you need to have a valid PLANQK account and a quantum access token.
This token is used to authenticate your requests to the PLANQK platform and to track the usage costs of your quantum executions.

Log in to [PLANQK](https://platform.planqk.de/home) and copy your personal access token to the clipboard.
Optionally, you may create a dedicated access token in your user [settings](https://platform.planqk.de/settings/access-tokens) that you can use for your Qiskit code.

Copy your new token and store it in a safe place.

## Backend Selection and Execution

In your Qiskit code you can access the PLANQK quantum backends through the `PlanqkQuantumProvider` object.
You need to import this object and pass your access token to it, as shown in the example below.

```python
from planqk.qiskit import PlanqkQuantumProvider

# set your PLANQK access token
planqk_token = "YOUR_ACCESS_TOKEN"
provider = PlanqkQuantumProvider(access_token=planqk_token)
```

::: tip NOTE
If your Qiskit code is executed in a PLANQK service, the access token is automatically set by the platform.
In this case the `access_token` parameter can be omitted.
If it is set it is replaced by the service token.
:::

After you have created the provider object you can list all backends supported by the PLANQK platform and select the one you want to use, e.g., the `azure.ionq.simulator` backend:

```python
# list all available PLANQK quantum backends
backends = planqk_provider.backends()

# select certain backend
backend = provider.get_backend("azure.ionq.simulator")
```

Now you can execute your Qiskit circuit on the selected backend, retrieve its `job` object, retrieve its results, cancel it etc.

```python
from qiskit import QuantumCircuit, transpile

# create a qiskit circuit
circuit = QuantumCircuit(3, 3)
circuit.h(0)
circuit.cx(0, 1)
circuit.cx(1, 2)
circuit.measure(range(3), range(3))

# transpile circuit for backend
circuit = transpile(circuit, backend)

# execute circuit on selected backend
job = backend.run(circuit, shots=1000)
```

::: tip NOTE
Executing your Qiskit code on the PLANQK platform may lead to execution costs depending on selected backend and number of shots.
Please find an overview about the costs for each backend on [our pricing page](https://platform.planqk.de/pricing).
:::
