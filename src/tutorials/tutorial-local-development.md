# Utilize the PLANQK Service SDK for Local Development

This tutorial provides step-by-step guidance on how to create services, monitor their statuses, retrieve their results, and cancel their executions locally. To accomplish this objective, the tutorial utilizes the PLANQK Service SDK and PLANQK CLI.

Prerequisites:
Ensure that Docker is installed and running properly. For detailed documentation, please refer to the following link: [Docker Desktop](https://www.docker.com/products/docker-desktop)

## Deploying the Services locally with the PLANQK CLI

To install the PLANQK CLI, you must install Node.js and the npm command line interface using either a
[Node version manager](https://github.com/nvm-sh/nvm) or a [Node installer](https://nodejs.org/en/download).

Then install the PLANQK CLI globally using npm:

```bash
npm install -g @anaqor/planqk
```

Once the installation is complete, start by navigating to the directory where your project, which includes the service, is located.

```bash
cd my-project
```

Next, run the following command:

```bash
planqk serve
```

Once the SERVICE is up and running, you can access its API under <http://localhost:8081>.
For additional details regarding the `planqk serve` functionality, please refer to the documentation available [here](../cli-reference#planqk-serve).

Alternatively, you can access the service from your Python code through the [PLANQK Service SDK](../sdk-reference-service).
