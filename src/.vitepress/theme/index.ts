import type {Theme} from "vitepress";
import DefaultTheme from "vitepress/theme";

import vitepressBackToTop from "vitepress-plugin-back-to-top";
import "vitepress-plugin-back-to-top/dist/style.css";

import "./planqk.css";
import "./my-fonts.css";
import "./colors.css";

import NextSection from "../components/NextSection.vue";
import LoomVideo from "../components/LoomVideo.vue";
import ImageShadow from "../components/ImageShadow.vue";


export default {
  extends: DefaultTheme,
  enhanceApp({app}) {
    vitepressBackToTop({
      threshold: 300,
    });

    app.component('NextSection', NextSection)
    app.component('LoomVideo', LoomVideo)
    app.component('ImageShadow', ImageShadow)
  },
} satisfies Theme;
