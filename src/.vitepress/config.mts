import {defineConfig} from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: 'PLANQK Docs',
  description: 'Documentation for the PLANQK Platform',

  ignoreDeadLinks: 'localhostLinks',

  head: [
    ['meta', {name: "theme-color", content: "#ffffff"}],
    ['link', {rel: "shortcut icon", href: "/favicon.ico"}],
    ['link', {rel: "stylesheet", href: "/material-symbols/outlined.css"}],
    ['link', {rel: "stylesheet", href: "/material-symbols/customization.css"}],
    ['link', {rel: "stylesheet", href: "/fontawesome/css/all.css"}],
    ['link', {rel: "stylesheet", href: "/colors.css"}],

    // Open Graph meta tags
    ['meta', {property: "og:url", content: "https://docs.planqk.de"}],
    ['meta', {property: "og:type", content: "website"}],
    ['meta', {property: "og:title", content: "PLANQK Docs"}],
    ['meta', {property: "og:description", content: "Documentation for the PLANQK Platform"}],
    ['meta', {property: "og:image", content: "/open_graph_dark.jpg"}],

    // Twitter meta tags
    ['meta', {property: "twitter:card", content: "summary_large_image"}],
    ['meta', {property: "twitter:domain", content: "docs.planqk.de"}],
    ['meta', {property: "twitter:url", content: "https://docs.planqk.de"}],
    ['meta', {property: "twitter:title", content: "PLANQK Docs"}],
    ['meta', {property: "twitter:description", content: "Documentation for the PLANQK Platform"}],
    ['meta', {property: "twitter:image", content: "/open_graph_dark.jpg"}],
  ],

  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    logo: {
      light: '/logo_light.png',
      dark: '/logo_dark.png'
    },
    siteTitle: false,

    nav: [
      {
        text: 'Login',
        link: 'https://platform.planqk.de/home'
      },
      {
        text: 'Sign Up',
        link: 'https://login.planqk.de/realms/planqk/protocol/openid-connect/registrations?client_id=planqk-login&response_type=code&scope=openid&redirect_uri=https%3A%2F%2Fplatform.planqk.de%2Fhome',
      },
      {
        text: 'Pricing',
        link: 'https://kipu-quantum.com/platform/pricing',
      },
      {
        text: 'Quantum Backends',
        link: 'https://platform.planqk.de/quantum-backends',
      }
    ],

    sidebar: {
      '/': [
        {
          text: 'Getting Started',
          collapsed: false,
          items: [
            {text: 'Quickstart', link: '/quickstart'},
            {text: 'Accessing Quantum Backends', link: '/using-sdk'},
            {text: 'Available Backends', link: 'https://platform.planqk.de/quantum-backends', target: 'blank'},
            {text: 'Quantum SDK Reference', link: '/sdk-reference'},
            {text: 'Service Access from Python', link: '/sdk-reference-service'},
            {text: 'CLI Reference', link: '/cli-reference'},
            {text: 'planqk.json Reference', link: '/planqk-json-reference'},
            {text: 'Manage Organizations', link: '/manage-organizations'},
            {text: 'Manage Access Tokens', link: '/manage-access-tokens'},
            {text: 'Manage Quantum Jobs', link: '/manage-quantum-jobs'},
          ]
        },
        {
          text: 'Implementations',
          collapsed: true,
          items: [
            {text: 'Introduction', link: '/implementations/introduction'},
            {text: 'Getting Started', link: '/implementations/getting-started'},
            {text: 'Implementation Settings', link: '/implementations/settings'},
            {text: 'Create a PLANQK Service', link: '/implementations/create-a-service'},
          ]
        },
        {
          text: 'Managed Services',
          collapsed: true,
          items: [
            {text: 'Introduction', link: '/services/managed/introduction'},
            {text: 'Runtime Interface', link: '/services/managed/runtime-interface'},
            {text: 'Service Configuration', link: '/services/managed/service-configuration'},
            {text: 'API Specification', link: '/services/managed/api-specification'},
            {text: 'Run as Jobs', link: '/services/managed/jobs'},
            {text: 'Custom Docker Containers', link: '/services/managed/custom-containers'},
          ]
        },
        {
          text: 'Using a Service',
          collapsed: true,
          items: [
            {text: 'Applications', link: '/services/applications'},
            {text: 'Using a Service', link: '/services/using-a-service'},
          ]
        },
        {
          text: 'Service Orchestration',
          collapsed: true,
          items: [
            {text: 'Introduction', link: '/services/orchestration/introduction'},
            {text: 'The Workflow Editor', link: '/services/orchestration/workflow-editor'},
            {text: 'A Workflow Example', link: '/services/orchestration/example'},

          ]
        },
        {
          text: 'On-Premise Services',
          collapsed: true,
          items: [
            {text: 'Introduction', link: '/services/on-premise/introduction'},
            {text: 'Publish on Marketplace', link: '/services/on-premise/publish-marketplace'},
            {text: 'Report Service Usage', link: '/services/on-premise/report-usage'},
          ]
        },
        {
          text: 'Demos',
          collapsed: true,
          items: [
            {text: 'Introduction', link: '/demos/introduction'},
            {text: 'Deploy a Demo', link: '/demos/deploy-demo'},
            {text: 'Set Environment Variables', link: '/demos/environment-variables'},
            {text: 'Starter Templates', link: '/demos/starter-templates'},
          ]
        },
        {
          text: 'Automation',
          collapsed: true,
          items: [
            {text: 'CI/CD Integration with PLANQK', link: '/automation/introduction'},
            {text: 'GitHub Workflows', link: '/automation/github'},
            {text: 'GitLab CI', link: '/automation/gitlab'},
          ]
        },
        {
          text: 'Tutorials',
          collapsed: true,
          items: [
            {text: 'Execute Qiskit Circuits using the PLANQK Quantum SDK', link: '/tutorials/tutorial-qiskit-with-planqk-sdk'},
            {text: 'Solving the Maximum Independent Set Problem on QuEra Aquila using the PLANQK Quantum SDK', link: '/tutorials/tutorial-quera-mis'},
            {text: 'Utilize the PLANQK Service SDK for Local Development', link: '/tutorials/tutorial-local-development'},
            {text: 'Create and Test an On-Premise Service', link: '/tutorials/tutorial-meter-on-premise-service'},
            {text: 'Access D-Wave Leap in a PLANQK Service', link: '/tutorials/tutorial-dwave'},
            {text: 'Access IBM Quantum Platform Backends in a PLANQK Service', link: '/tutorials/tutorial-ibmq'},
            {text: 'Use Qiskit Runtime in a PLANQK Service', link: '/tutorials/tutorial-qiskit-runtime'},
          ]
        },
        {
          text: 'Community',
          collapsed: true,
          items: [
            {text: 'Introduction', link: '/community/overview'},
            {text: 'Algorithms', link: '/community/algorithms'},
            {text: 'Data Pools', link: '/community/data-pools'},
            {text: 'Use Cases', link: '/community/use-cases'},
            {text: 'Markdown & LaTeX', link: '/community/markdown-latex-editor'},
            {text: 'Manage Permissions', link: '/community/manage-permissions'},
            {text: 'Publishing Content', link: '/community/publish-content'},
            {text: 'Reviews', link: '/community/reviews'},
          ]
        },
      ],
    },

    search: {
      provider: 'local'
    },

    socialLinks: [],

    footer: {
      message: 'Published under the CC-BY-4.0 License.',
      copyright: 'Copyright © 2023-present | Kipu Quantum GmbH'
    },

    lastUpdated: true,
    externalLinkIcon: true,

    editLink: {
      pattern: 'https://gitlab.com/planqk-foss/planqk-docs/-/edit/main/src/:path',
      text: 'Help us improve this page!'
    }
  }
});
