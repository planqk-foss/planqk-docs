---
aside: false
---

# Introduction

:wave: Welcome to the PLANQK Documentation!

Here you find all the resources to get you on your way with the PLANQK Platform.

### Got a Question or Problem?

We recommend joining our [Discord Server](https://discord.gg/qhwDBPpuFE) to ask support-related questions, report bugs, or request new features.

### Stay Up-to-Date

Join our [Discord Server](https://discord.gg/qhwDBPpuFE) to receive updates about new exciting platform features.
