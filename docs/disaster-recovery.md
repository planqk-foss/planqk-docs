# Disaster Recovery

The application is deployed to GitLab Pages, which is automatically done by the CI/CD pipeline for every commit to the `main` branch.

To serve the application on a custom domain, the following steps are required:

1. Create a new CNAME DNS record for the custom domain pointing to the GitLab Pages domain (`planqk-foss.gitlab.io`)
    - `docs.platform.planqk.de`
    - `docs.planqk.de`
2. In the GitLab project settings, add the custom domain to the Pages domain list
3. Follow the instructions in the GitLab Pages settings to verify the domain ownership
